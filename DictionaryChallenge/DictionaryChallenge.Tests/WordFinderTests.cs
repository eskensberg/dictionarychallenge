using System;
using System.Collections.Generic;
using System.Linq;
using DictionaryChallenge.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace DictionaryChallenge.Tests
{
    public class WordFinderTests
    {
        [Fact]
        public void QueryingForCharacterDifferences_ReturnsAndIndexArray_WhenComparingWords() // first requirement
        {
            var wordFinderBuilder = new WordFinderBuilder().IncludeDictionary().Build();

            var differences = wordFinderBuilder.WordFinderService.FindDifferences("spin", "spot");

            differences.Length.Should().Be(2);
            differences.First().Should().Be(2);
            differences.Last().Should().Be(3);
        }

        [Fact]
        public void FindShortestLetterChangeForWords_ShouldReturnResult_WhenQuerying()
        {
            var wordFinderBuilder = new WordFinderBuilder().IncludeDictionary().Build();

            var changeArray = wordFinderBuilder.WordFinderService.FindShortestLetterChangesForWordsAsList("spin", "spot");

            var expectedResult = new List<string> { "spin", "spit", "spot" };

            changeArray.Count.Should().Be(3);

            for (var i = 0; i < changeArray.Count; i++)
            {
                changeArray[i].Should().Be(expectedResult[i]);
            }
        }

        [Fact]
        public void FindShortestLetterChangeForWords_ShouldReturnResult_WhenQuerying_2() // first requirement
        {
            var wordFinderBuilder = new WordFinderBuilder().IncludeDictionary().Build();

            var changeArray = wordFinderBuilder.WordFinderService.FindShortestLetterChangesForWordsAsList("zest", "ball");

            var expectedResult = new List<string> { "zest", "best", "belt", "bell", "ball" };

            changeArray.Count.Should().Be(5);

            for (var i = 0; i < changeArray.Count; i++)
            {
                changeArray[i].Should().Be(expectedResult[i]);
            }
        }

        [Fact]
        public void FindShortestLetterChangeForWords_ShouldReturnTwoWords_WhenQuerying()
        {
            var wordFinderBuilder = new WordFinderBuilder().IncludeDictionary().Build();

            var changeArray = wordFinderBuilder.WordFinderService.FindShortestLetterChangesForWordsAsList("spin", "spit");

            var expectedResult = new List<string> { "spin", "spit" };

            changeArray.Count.Should().Be(2);

            for (var i = 0; i < changeArray.Count; i++)
            {
                changeArray[i].Should().Be(expectedResult[i]);
            }
        }

        [Fact]
        public void FindShortestLetterChangeForWords_ShouldReturnNoMatchesFound_WhenQuerying()
        {
            var wordFinderBuilder = new WordFinderBuilder().IncludeDictionary().Build();

            var changeArray = wordFinderBuilder.WordFinderService.FindShortestLetterChangesForWordsAsList("spin", "1pi2");

            changeArray.Count.Should().Be(1);

            changeArray.FirstOrDefault().Should().NotBeNullOrEmpty().And.Be("no-matches-found");
        }
    }
}
