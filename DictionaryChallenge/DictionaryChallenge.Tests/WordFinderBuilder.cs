using System;
using System.Linq;
using DictionaryChallenge.Services;
using Moq;

namespace DictionaryChallenge.Tests
{
    public class WordFinderBuilder
    {
        public Mock<IDictionaryFetcherService> MockDictionary { get; } = new Mock<IDictionaryFetcherService>();
        public WordFinderServiceService WordFinderService { get; set; }

        public WordFinderBuilder IncludeDictionary()
        {
            var wordsListSizeOfFourLetters = Properties.Resources.DictionaryList.Split(Environment.NewLine).ToList();

            MockDictionary
                .Setup(x => x.GetWords(It.IsAny<string>(), It.IsAny<int>()))
                .ReturnsAsync(wordsListSizeOfFourLetters);

            return this;
        }

        public WordFinderBuilder Build()
        {
            WordFinderService = new WordFinderServiceService(MockDictionary.Object, "");

            return this;
        }
    }
}