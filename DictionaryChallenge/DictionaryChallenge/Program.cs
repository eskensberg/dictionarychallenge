﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DictionaryChallenge.Services;

namespace DictionaryChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
           
            var fileName = GetFileName();

            var serviceProvider = BuildServiceProvider(fileName);

            var wordFinderService = serviceProvider.GetService<IWordFinderService>();

            var runningTheProgram = true;
            while (runningTheProgram)
            {
                var startWord = GetStartWord();

                var endWord = GetEndWord();

                var resultsFile = GetResultFileName();

                var changesList = wordFinderService.FindShortestLetterChangesForWordsAsList(startWord, endWord);

                DisplayAndSaveResults(changesList, resultsFile);

                runningTheProgram = WouldUserLikeToRunTheQueryAgain();
            }

            Console.WriteLine("Existing..." + Environment.NewLine);
        }

        private static ServiceProvider BuildServiceProvider(string fileName)
        {
            var serviceProviderCollection = new ServiceCollection()
                .AddSingleton<IDictionaryFetcherService, DictionaryFetcherServiceService>();

            serviceProviderCollection
                .AddSingleton<IWordFinderService>(
                    x => new WordFinderServiceService(x.GetRequiredService<IDictionaryFetcherService>(), $@"{AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"))}Words\{fileName}.txt"));

            return serviceProviderCollection.BuildServiceProvider();
        }

        private static bool WouldUserLikeToRunTheQueryAgain()
        {
            Console.WriteLine($"Would you like to run the query again? Y/N?");

            while (true)
            {
                ConsoleKeyInfo input = Console.ReadKey();
                Console.WriteLine();
                if (input.Key == ConsoleKey.Escape || input.KeyChar.ToString().ToLower() == "n")
                {
                    return false;
                }
                if (input.KeyChar.ToString().ToLower() == "y")
                {
                    return true;
                }
            }
        }

        private static void DisplayAndSaveResults(List<string> changesList, string resultsFile)
        {
            try
            {
                using (var writer = new StreamWriter($@"{AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"))}Words\{resultsFile}.txt"))
                {
                    foreach (var word in changesList)
                    {
                        writer.WriteLine(word);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(Environment.NewLine + " - - - FAILED TO SAVE - - -" + Environment.NewLine);
                Console.WriteLine(e + Environment.NewLine);
            }
          

            Console.WriteLine();
            Console.WriteLine(" - - - SOLUTION - - -");
            Console.WriteLine();

            foreach (var word in changesList)
            {
                Console.WriteLine(word);
            }

            Console.WriteLine();
            Console.WriteLine(" - - - - - - - - - - -");
            Console.WriteLine();
        }

        private static string GetFileName()
        {
            Console.WriteLine(@"Please type the name of the dictionary file or press ENTER for default file location at ");
            Console.WriteLine($@"{AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"))}Words\words-english.txt");

            var fileName = "words-english";

            string input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                Console.WriteLine($"input:{input}");
                fileName = input;
            }

            return fileName;
        }

        private static string GetResultFileName()
        {
            Console.WriteLine(@"Please type the name of results file or press ENTER for default file location at ");
            Console.WriteLine($@"{AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"))}Words\Results.txt");

            var fileName = "Results";

            string input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                Console.WriteLine($"ResultFile:{input}{Environment.NewLine}");
                fileName = input;
            }

            return fileName;
        }

        private static string GetEndWord()
        {
            Console.WriteLine($"please enter a 4 letter endWord:");
            var endWord = Console.ReadLine();

            while (string.IsNullOrEmpty(endWord) || endWord.Length != 4)
            {
                Console.WriteLine($"please enter a 4 letter endWord:");
                endWord = Console.ReadLine();
            }

            return endWord;
        }

        private static string GetStartWord()
        {
            Console.WriteLine($"please enter a 4 letter startWord:");
            var startWord = Console.ReadLine();

            while (string.IsNullOrEmpty(startWord) || startWord.Length != 4)
            {
                Console.WriteLine($"please enter a 4 letter startWord:");
                startWord = Console.ReadLine();
            }

            return startWord;
        }
    }
}
