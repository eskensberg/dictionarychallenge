﻿using System.Collections.Generic;

namespace DictionaryChallenge.Services
{
    public interface IWordFinderService
    {
        List<string> FindShortestLetterChangesForWordsAsList(string startWord, string endWord);
    }
}