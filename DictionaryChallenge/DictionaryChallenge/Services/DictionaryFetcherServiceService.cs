﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DictionaryChallenge.Services
{
    public class DictionaryFetcherServiceService : IDictionaryFetcherService
    { 
        public async Task<List<string>> GetWords(string fileName, int length = 4)
        {
            var words = new List<string>();

            using (StreamReader reader = new StreamReader(fileName))
            {
                string word;
                while ((word = await reader.ReadLineAsync()) != null)
                {
                    if (word.Length == length)
                    {
                        words.Add(word);
                    }
                }
            }

            Console.WriteLine($"Fetching {words.Count} words from dictionary.{Environment.NewLine}");
            return words;
        }
    }
}