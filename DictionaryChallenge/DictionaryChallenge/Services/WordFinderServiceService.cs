﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DictionaryChallenge.Tests")]
namespace DictionaryChallenge.Services
{
    public class WordFinderServiceService : IWordFinderService
    {
        private readonly List<string> _dictionary;

        public WordFinderServiceService(IDictionaryFetcherService dictionaryFetcher, string fileName)
        {
            _dictionary = dictionaryFetcher.GetWords(fileName).Result;
        }

        internal int[] FindDifferences(string startWord, string endWord)
        {
            var differentIndexes = new List<int>();

            for (var i = 0; i < startWord.Length; i++)
            {
                if (startWord[i] != endWord[i])
                {
                    differentIndexes.Add(i);
                }
            }

            return differentIndexes.ToArray();
        }

        public List<string> FindShortestLetterChangesForWordsAsList(string startWord, string endWord)
        {
            var changeArray = FindShortestLetterChangesForWords(startWord, endWord);

            if (changeArray.Length == 1)
            {
                return new List<string> { $"{changeArray.FirstOrDefault()}" };
            }
            if (changeArray.Length == 2)
            {
                return new List<string> { startWord, endWord };
            }

            var list = new List<string> { changeArray.First() };

            for (int i = 1; i < changeArray.Length; i++)
            {
                list.Add(changeArray[i].Split(':')[1]);
            }

            return list;
        }

        public string[] FindShortestLetterChangesForWords(string startWord, string endWord)
        {
            var differencesIndex = FindDifferences(startWord, endWord);

            if (differencesIndex.Length == 0)
            {
                return new[] { startWord };
            }

            if (differencesIndex.Length == 1)
            {
                return new[] { startWord, $"{differencesIndex.First()}:{endWord}" };
            }

            List<string> solution = new List<string>();

            solution.Add(startWord);

            var matchedWords = new List<List<string>>();

            foreach (var word in _dictionary)
            {
                var ignoreList = new List<string> { startWord, endWord };
                if (!ignoreList.Contains(word))
                {
                    ignoreList.Add(word);

                    var result = FindShortestLetterChangesForWords(word, startWord, endWord, ignoreList);

                    if (result.Any())
                    {
                        matchedWords.Add(result);
                    }
                    if (result.Count == 1) // smallest value found
                    {
                        break;
                    }
                }
            }

            if (matchedWords.Any())
            {
                var smallestResult = matchedWords.OrderBy(x => x.Count).FirstOrDefault(x => x.Count >= 1);

                foreach (var result in smallestResult)
                {
                    solution.Add(result);
                }
                return solution.ToArray();
            }

            return new[] { "no-matches-found" };
        }


        private List<string> FindShortestLetterChangesForWords(string dictionaryWord, string startWord, string endWord, List<string> ignoreListForDictionary)
        {
            var differencesIndex = FindDifferences(startWord, endWord);

            if (differencesIndex.Length <= 1)
            {
                return new List<string> { $"{differencesIndex.First()}:{endWord}" };
            }

            var differencesBetweenStartAndNewWord = FindDifferences(startWord, dictionaryWord);

            if (DoTheWordsDifferByOneLetterThatMatchesEndWordLettter(dictionaryWord, endWord, differencesBetweenStartAndNewWord))
            {
                return new List<string>();
            }

            var solution = new List<string> { $"{differencesBetweenStartAndNewWord.First()}:{dictionaryWord}" };
            if (differencesIndex.Length == 2)
            {
                differencesIndex = FindDifferences(dictionaryWord, endWord);
                solution.Add($"{differencesIndex.First()}:{endWord}");
            }
            else
            {
                var matchedWords = new List<List<string>>();

                foreach (var word in _dictionary)
                {
                    if (!ignoreListForDictionary.Contains(word))
                    {
                        var ignoreList = new List<string> { word };
                        ignoreList.AddRange(ignoreListForDictionary);

                        var result = FindShortestLetterChangesForWords(word, dictionaryWord, endWord, ignoreList);

                        if (result.Any())
                        {
                            matchedWords.Add(result);
                        }
                        if (result.Count == 1) // smallest value found
                        {
                            break;
                        }
                    }
                }


                if (matchedWords.Any())
                {
                    var smallestResult = matchedWords.OrderBy(x => x.Count).FirstOrDefault(x => x.Count >= 1);

                    foreach (var result in smallestResult)
                    {
                        solution.Add(result);
                    }
                }
            }
         

            return solution;
        }

        private bool DoTheWordsDifferByOneLetterThatMatchesEndWordLettter(string dictionaryWord, string endWord, int[] differencesBetweenStartAndNewWord)
        {
            // looking for 1 letter difference at a time only
            // different letter is at the index where change is required  
            // and letter belongs to the endWord
            // if condition is NOT met, then return empty List<string>
            if (differencesBetweenStartAndNewWord.Length != 1 ||
                endWord[differencesBetweenStartAndNewWord.First()] !=
                dictionaryWord[differencesBetweenStartAndNewWord.First()])
            {
                return true;
            }

            return false;
        }
    }
}