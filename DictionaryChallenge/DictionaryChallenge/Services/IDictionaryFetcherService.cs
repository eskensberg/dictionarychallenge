﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DictionaryChallenge.Services
{
    public interface IDictionaryFetcherService
    {
        Task<List<string>> GetWords(string fileName, int length = 4);
    }
}